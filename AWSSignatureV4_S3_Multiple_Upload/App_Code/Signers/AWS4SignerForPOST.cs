﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AWSSignatureV4_S3_Upload_Verification.Signers
{
    /// <summary>
    /// Sample AWS4 signer demonstrating how to sign POST requests to Amazon S3
    /// using a policy.
    /// </summary>
    public class AWS4SignerForPOST : AWS4SignerBase
    {
        public string FormatCredentialStringForPolicy(string awsAccessKey, DateTime dateTimeStamp)
        {
            var dateStamp = dateTimeStamp.ToString(DateStringFormat, CultureInfo.InvariantCulture);
            string credentialString = "AKIAIOSFODNN7EXAMPLE/20130806/cn-north-1/s3/aws4_request";
            var scope = string.Format("{0}/{1}/{2}/{3}",
                          dateStamp,
                          Region,
                          Service,
                          TERMINATOR);
            credentialString = string.Format("{0}/{1}", awsAccessKey, scope );

            return credentialString;                
        }

        public string FormatAlgorithmForPolicy
        {
            get { return "AWS4-HMAC-SHA256"; }
        }

        public string FormatDateTimeForPolicy(DateTime dateTimeStamp)
        {
            return dateTimeStamp.ToString(ISO8601BasicFormat, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Computes an AWS4 signature for a request, ready for inclusion as an 
        /// 'Authorization' header.
        /// </summary>
        /// <param name="headers">
        /// The request headers; 'Host' and 'X-Amz-Date' will be added to this set.
        /// </param>
        /// <param name="queryParameters">
        /// Any query parameters that will be added to the endpoint. The parameters 
        /// should be specified in canonical format.
        /// </param>
        /// <param name="bodyHash">
        /// Precomputed SHA256 hash of the request body content; this value should also
        /// be set as the header 'X-Amz-Content-SHA256' for non-streaming uploads.
        /// </param>
        /// <param name="awsAccessKey">
        /// The user's AWS Access Key.
        /// </param>
        /// <param name="awsSecretKey">
        /// The user's AWS Secret Key.
        /// </param>
        /// <param name="requestDateTime">
        /// The curent date timestamp.
        /// </param>
        /// <param name="AWSCredientialsV4">
        /// The Hashtable to store the credentials.
        /// </param>
        /// <returns>
        /// None.  The AWSCredientialsV4 Hashtable is populated with the results
        /// </returns>
        public void ComputeSignature(IDictionary<string, string> headers,
                                       string queryParameters,
                                       string base64PolicyStringBodyHash,
                                       string awsAccessKey,
                                       string awsSecretKey,
                                       DateTime requestDateTime,
                                       List<PostS3AWSPolicyCondition> addedConditions)
        {
            // first get the date and time for the subsequent request, and convert to ISO 8601 format
            // for use in signature generation
            var dateTimeStamp = requestDateTime.ToString(ISO8601BasicFormat, CultureInfo.InvariantCulture);

            // update the headers with required 'x-amz-date' and 'host' values
            headers.Add(X_Amz_Date, dateTimeStamp);

            var hostHeader = EndpointUri.Host;
            if (!EndpointUri.IsDefaultPort)
                hostHeader += ":" + EndpointUri.Port;
            headers.Add("Host", hostHeader);

            // canonicalize the headers; we need the set of header names as well as the
            // names and values to go into the signature process
            var canonicalizedHeaderNames = CanonicalizeHeaderNames(headers);
            var canonicalizedHeaders = CanonicalizeHeaders(headers);

            // if any query string parameters have been supplied, canonicalize them
            // (note this sample assumes any required url encoding has been done already)
            var canonicalizedQueryParameters = string.Empty;
            if (!string.IsNullOrEmpty(queryParameters))
            {
                var paramDictionary = queryParameters.Split('&').Select(p => p.Split('='))
                                                     .ToDictionary(nameval => nameval[0],
                                                                   nameval => nameval.Length > 1
                                                                        ? nameval[1] : "");

                var sb = new StringBuilder();
                var paramKeys = new List<string>(paramDictionary.Keys);
                paramKeys.Sort(StringComparer.Ordinal);
                foreach (var p in paramKeys)
                {
                    if (sb.Length > 0)
                        sb.Append("&");
                    sb.AppendFormat("{0}={1}", p, paramDictionary[p]);
                }

                canonicalizedQueryParameters = sb.ToString();
            }

            // canonicalize the various components of the request
            var canonicalRequest = CanonicalizeRequest(EndpointUri,
                                                       HttpMethod,
                                                       canonicalizedQueryParameters,
                                                       canonicalizedHeaderNames,
                                                       canonicalizedHeaders,
                                                       base64PolicyStringBodyHash);
            Console.WriteLine("\nCanonicalRequest:\n{0}", canonicalRequest);

            // generate a hash of the canonical request, to go into signature computation
            var canonicalRequestHashBytes
                = CanonicalRequestHashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(canonicalRequest));

            // construct the string to be signed
            var stringToSign = new StringBuilder();

            var dateStamp = requestDateTime.ToString(DateStringFormat, CultureInfo.InvariantCulture);
            var scope = string.Format("{0}/{1}/{2}/{3}",
                                      dateStamp,
                                      Region,
                                      Service,
                                      TERMINATOR);

            string algorithm = SCHEME + "-" + ALGORITHM;
            stringToSign.AppendFormat("{0}\n{1}\n{2}\n", algorithm, dateTimeStamp, scope);
            stringToSign.Append(ToHexString(canonicalRequestHashBytes, true));

            Console.WriteLine("\nStringToSign:\n{0}", stringToSign);

            // compute the signing key
            var kha = KeyedHashAlgorithm.Create(HMACSHA256);
            kha.Key = DeriveSigningKey(HMACSHA256, awsSecretKey, Region, dateStamp, Service);

            // compute the AWS4 signature and return it
            // https://stackoverflow.com/questions/45109865/generating-an-aws-signature-v4-signature-for-uploading-to-s3 
            //var signature = kha.ComputeHash(Encoding.UTF8.GetBytes(stringToSign.ToString()));
            var signature = kha.ComputeHash(Encoding.UTF8.GetBytes(base64PolicyStringBodyHash));
            var signatureString = ToHexString(signature, true);
            Console.WriteLine("\nSignature:\n{0}", signatureString);

            var authString = new StringBuilder();
            authString.AppendFormat("{0}-{1} ", SCHEME, ALGORITHM);
            string credential = awsAccessKey + "/" + scope;
            authString.AppendFormat("Credential={0}, ", credential);
            authString.AppendFormat("SignedHeaders={0}, ", canonicalizedHeaderNames);
            authString.AppendFormat("Signature={0}", signatureString);

            var authorization = authString.ToString();
            Console.WriteLine("\nAuthorization:\n{0}", authorization);

            // Add credentials to form input list
            addedConditions.Add(new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, "Policy", base64PolicyStringBodyHash));
            addedConditions.Add(new PostS3AWSPolicyCondition(PostS3AWSPolicyCondition.MATCHTYPE_EXACT, AWS4SignerBase.X_Amz_Signature, signatureString));
        }
    }
}
