﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// A class to hold an AWS POST Policy condition https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTConstructPolicy.html 
/// </summary>
public class PostS3AWSPolicyCondition
{
    public static string MATCHTYPE_EXACT = "";  // Exact
    public static string MATCHTYPE_STARTS_WITH = "starts-with";
    public static string MATCHTYPE_RANGE = "content-length-range";

    public string matchType;
    public string name;
    public string value;
    public Boolean includeInHtmlForm;

    public PostS3AWSPolicyCondition(string matchType, string name, string value)
    {
        this.matchType = matchType;
        this.name = name;
        this.value = value;
        this.includeInHtmlForm = true;
    }

    public PostS3AWSPolicyCondition(string matchType, string name, string value, Boolean includeInHtmlForm)
    {
        this.matchType = matchType;
        this.name = name;
        this.value = value;
        this.includeInHtmlForm = includeInHtmlForm;
    }


}