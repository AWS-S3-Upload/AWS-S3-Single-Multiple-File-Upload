﻿<!--Reference eddturtle-->
<!--https://github.com/eddturtle/direct-upload-s3-signaturev4/blob/master/index.php-->
<!--https://www.designedbyaturtle.co.uk/2015/direct-upload-to-s3-using-aws-signature-v4-php/-->

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Direct Upload Example</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" />
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <div style="top: 20%; left: 40%; width: 500px; position: absolute;">
        <h1>Multiple File Upload</h1>
        <form id="uploadpage" runat="server" method="POST" enctype="multipart/form-data" class="direct-upload">
            <table class="tblmain">
                <tr>

                    <td>Username
                    </td>
                    <td>
                        <asp:TextBox ID="txtusername" Text="user" runat="server"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td>Project Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtprojectname" Text="user1" runat="server"></asp:TextBox>
                    </td>

                </tr>
            </table>
            <table class="tblmain2">
                <tr>
                    <td colspan="2">
                        <!-- The elements after this will be ignored -->
                        <div id="selectedFiles"></div>
                    </td>
                </tr>
                <tr>
                    <div id="box" runat="server">
                        <td colspan="2">
                            <asp:Button ID="btnUpload" runat="server" Text="Submit" OnClick="btnUpload_Click" />
                        </td>
                    </div>
                </tr>
                <tr>
                    <!-- Progress Bars to show upload completion percentage -->
                    <div class="progress-bar-area"></div>
                </tr>
            </table>
        </form>
        <!-- This area will be filled with our results (for debugging) -->
        <div>
            <h3>Files</h3>
            <textarea id="uploaded"></textarea>
        </div>
    </div>
    <!--<script src="jquery-3.2.1.min.js"></script>-->
    <!-- Start of the JavaScript -->
    <!-- Load jQuery & jQuery UI (Needed for the FileUpload Plugin) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <!-- Load the FileUpload Plugin (more info @ https://github.com/blueimp/jQuery-File-Upload) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.14.1/js/jquery.fileupload.min.js"></script>

    <script>
        $(document).ready(function () {
            // Assigned to variable for later use.
            var form = $('.direct-upload');
            var filesUploaded = [];
            // Place any uploads within the descending folders
            // so ['test1', 'test2'] would become /test1/test2/filename
            var folders = [];
            form.fileupload({
                url: form.attr('action'),
                type: form.attr('method'),
                datatype: 'xml',
                add: function (event, data) {
                    // Show warning message if your leaving the page during an upload.
                    window.onbeforeunload = function () {
                        return 'You have unsaved changes.';
                    };
                    // Give the file which is being uploaded it's current content-type (It doesn't retain it otherwise)
                    // and give it a unique name (so it won't overwrite anything already on s3).
                    var file = data.files[0];
                    var user = document.getElementById('txtusername').value;
                    var project = document.getElementById('txtprojectname').value;
                    var filename = user + "/" + project + "/" + file.name;
                    form.find('input[name="Content-Type"]').val(file.type);
                    form.find('input[name="key"]').val((folders.length ? folders.join('/') + '/' : '') + filename);
                    // Actually submit to form to S3.
                    data.submit();
                    // Show the progress bar
                    // Uses the file size as a unique identifier
                    var bar = $('<div class="progress" data-mod="' + file.size + '"><div class="bar"></div></div>');
                    $('.progress-bar-area').append(bar);
                    bar.slideDown('fast');
                },
                progress: function (e, data) {
                    // This is what makes everything really cool, thanks to that callback
                    // you can now update the progress bar based on the upload progress.
                    var percent = Math.round((data.loaded / data.total) * 100);
                    $('.progress[data-mod="' + data.files[0].size + '"] .bar').css('width', percent + '%').html(percent + '%');
                },
                fail: function (e, data) {
                    // Remove the 'unsaved changes' message.
                    window.onbeforeunload = null;
                    $('.progress[data-mod="' + data.files[0].size + '"] .bar').css('width', '100%').addClass('red').html('');
                },
                done: function (event, data) {
                    window.onbeforeunload = null;
                    // Upload Complete, show information about the upload in a textarea
                    // from here you can do what you want as the file is on S3
                    // e.g. save reference to your server using another ajax call or log it, etc.
                    var original = data.files[0];
                    var s3Result = data.result.documentElement.children;
                    filesUploaded.push({
                        "original_name": original.name,
                        "s3_name": s3Result[2].innerHTML,
                        "size": original.size,
                        "url": s3Result[0].innerHTML.replace("%2F", "/").replace("%2F", "/")
                    });
                    $('#uploaded').html(JSON.stringify(filesUploaded, null, 2));
                }
            });
        });
    </script>
</body>
</html>
